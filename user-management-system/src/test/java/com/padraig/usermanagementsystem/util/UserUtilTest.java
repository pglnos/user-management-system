package com.padraig.usermanagementsystem.util;

import com.padraig.usermanagementsystem.entity.UserEntity;
import com.padraig.usermanagementsystem.model.UserModel;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * User Util Test Class
 *
 * @author Padraig Glynn O'Shaughnessy (pglynnoshaughnessy@gmail.com)
 * @since 01/2021
 */
@DisplayName("User Util Test Class: ")
class UserUtilTest {

    @Test
    @DisplayName("Model to Entity Test")
    void modelToEntity() {

        UserModel userModel = new UserModel("pglynnoshaughnessy@gmail.com", "Padraig", "123");
        UserEntity userEntity = new UserEntity("pglynnoshaughnessy@gmail.com", "Padraig", "123", null);

        assertEquals(userEntity, UserUtil.modelToEntity(userModel));
    }

    @Test
    @DisplayName("Get Time and Date Test")
    void getTimeAndDate() {

        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
        LocalDateTime now = LocalDateTime.now();

        assertEquals(dtf.format(now), UserUtil.getTimeAndDate());
    }
}