package com.padraig.usermanagementsystem.repository;

import com.padraig.usermanagementsystem.entity.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * User Repository Interface
 *
 * @author Padraig Glynn O'Shaughnessy (pglynnoshaughnessy@gmail.com)
 * @since 01/2021
 */
@Repository
public interface UserRepository extends JpaRepository<UserEntity, Long> {

    UserEntity findByEmail(String userEmail);

    String deleteByEmail(String userEmail);
}
