package com.padraig.usermanagementsystem.util;

import com.padraig.usermanagementsystem.entity.UserEntity;
import com.padraig.usermanagementsystem.model.UserModel;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * User Util Class
 *
 * @author Padraig Glynn O'Shaughnessy (pglynnoshaughnessy@gmail.com)
 * @since 01/2021
 */
@Component
public class UserUtil {

    private UserUtil() {
    }

    public static UserEntity modelToEntity(UserModel userModel) {

        UserEntity userEntity = new UserEntity();

        userEntity.setEmail(userModel.getEmail());
        userEntity.setName(userModel.getName());
        userEntity.setPassword(userModel.getPassword());

        return userEntity;
    }

    public static String getTimeAndDate() {

        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
        LocalDateTime now = LocalDateTime.now();

        return dtf.format(now);
    }
}
