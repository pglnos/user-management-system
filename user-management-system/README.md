# Read Me First:

User-Management-System 1.0.0

- Spring-Boot 2.4.2
- Maven
- Java 11
- H2 Database
- Swagger UI
- 70% Unit Testing Line Coverage

# Getting Started:

- The application will run on the following port in the browser: localhost:8080

- The Swagger UI is available at: localhost:8080/swagger-ui/index.html

 - The H2-Database UI is available at: localhost:8080/h2-console

# H2 Database Credentials:

- H2 url: jdbc:h2:mem:e4b6cc8f-d750-45a1-80f5-34ee42dcb98f

- H2 username: sa

- H2 password: 