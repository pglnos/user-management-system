package com.padraig.usermanagementsystem.controller;

import com.padraig.usermanagementsystem.entity.UserEntity;
import com.padraig.usermanagementsystem.model.UserLoginModel;
import com.padraig.usermanagementsystem.model.UserModel;
import com.padraig.usermanagementsystem.service.UserService;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

/**
 * User Controller Test Class
 *
 * @author Padraig Glynn O'Shaughnessy (pglynnoshaughnessy@gmail.com)
 * @since 01/2021
 */
@ExtendWith(MockitoExtension.class)
@DisplayName("User Controller Test Class: ")
class UserControllerTest {

    @InjectMocks
    UserController userController;

    @Mock
    UserService userService;

    private final String userEmail = "pglynnoshaughnessy@gmail.com";
    private final UserModel userModel = new UserModel("pglynnoshaughnessy@gmail.com", "Padraig", "123");
    private final UserLoginModel userLoginModel = new UserLoginModel("pglynnoshaughnessy@gmail.com", "123");
    private final UserEntity userEntity = new UserEntity("pglynnoshaughnessy@gmail.com", "Padraig", "123", null);
    private final List<UserEntity> userList = new ArrayList<>();

    @Test
    void addUser() {

        when(userService.addUser(userModel)).thenReturn(true);
        assertEquals(new ResponseEntity(userModel, new HttpHeaders(), HttpStatus.CREATED), userController.addUser(userModel));
    }

    @Test
    void editUser() {

        when(userService.editUser(userModel)).thenReturn(true);
        assertEquals(new ResponseEntity(userModel, new HttpHeaders(), HttpStatus.OK), userController.editUser(userModel));
    }

    @Test
    void deleteUser() {

        when(userService.deleteUser(userEmail)).thenReturn(true);
        assertEquals(new ResponseEntity(userEmail, new HttpHeaders(), HttpStatus.OK), userController.deleteUser(userEmail));
    }

    @Test
    void getUser() {

        when(userService.getUser(userEmail)).thenReturn(userEntity);
        assertEquals(new ResponseEntity(userEntity, new HttpHeaders(), HttpStatus.OK), userController.getUser(userEmail));
    }

    @Test
    void listUsers() {

        userList.add(userEntity);
        when(userService.listUsers()).thenReturn(userList);
        assertEquals(new ResponseEntity(userList, new HttpHeaders(), HttpStatus.OK), userController.listUsers());
    }

    @Test
    void userLogin() {

        when(userService.userLogin(userLoginModel)).thenReturn(true);
        assertEquals(new ResponseEntity(userLoginModel, new HttpHeaders(), HttpStatus.OK), userController.userLogin(userLoginModel));
    }
}