package com.padraig.usermanagementsystem.controller;

import com.padraig.usermanagementsystem.constant.UserManagementSystemConstants;
import com.padraig.usermanagementsystem.entity.UserEntity;
import com.padraig.usermanagementsystem.model.UserLoginModel;
import com.padraig.usermanagementsystem.model.UserModel;
import com.padraig.usermanagementsystem.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * User Controller Class
 *
 * @author Padraig Glynn O'Shaughnessy (pglynnoshaughnessy@gmail.com)
 * @since 01/2021
 */
@Slf4j
@RestController
@RequestMapping("user")
public class UserController {

    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping("/addUser")
    public ResponseEntity<UserModel> addUser(@RequestBody UserModel userModel) {

        boolean requestComplete = false;

        try {
            requestComplete = userService.addUser(userModel);
        } catch (Exception e) {
            log.error("Unable to add User: " + userModel.getEmail() + " - " + e.getMessage());
        }

        if (requestComplete) {
            log.info(UserManagementSystemConstants.CREATED);
            return new ResponseEntity<>(userModel, new HttpHeaders(), HttpStatus.CREATED);
        } else {
            log.warn(UserManagementSystemConstants.BAD_REQUEST);
            return new ResponseEntity<>(userModel, new HttpHeaders(), HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping("/editUser")
    public ResponseEntity<UserModel> editUser(@RequestBody UserModel userModel) {

        boolean requestComplete = false;

        try {
            requestComplete = userService.editUser(userModel);
        } catch (Exception e) {
            log.error("Unable to edit User: " + userModel.getEmail() + " - " + e.getMessage());
        }

        if (requestComplete) {
            log.info(UserManagementSystemConstants.OK);
            return new ResponseEntity<>(userModel, new HttpHeaders(), HttpStatus.OK);
        } else {
            log.warn(UserManagementSystemConstants.BAD_REQUEST);
            return new ResponseEntity<>(userModel, new HttpHeaders(), HttpStatus.BAD_REQUEST);
        }
    }

    @DeleteMapping("/deleteUser/{user_email}")
    public ResponseEntity<String> deleteUser(@PathVariable(value = "user_email") String userEmail) {

        boolean requestComplete = false;

        try {
            requestComplete = userService.deleteUser(userEmail);
        } catch (Exception e) {
            log.error("Unable to delete User: " + userEmail + " - " + e.getMessage());
        }

        if (requestComplete) {
            log.info(UserManagementSystemConstants.OK);
            return new ResponseEntity<>(userEmail, new HttpHeaders(), HttpStatus.OK);
        } else {
            log.warn(UserManagementSystemConstants.BAD_REQUEST);
            return new ResponseEntity<>(userEmail, new HttpHeaders(), HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/getUser/{user_email}")
    public ResponseEntity<String> getUser(@PathVariable(value = "user_email") String userEmail) {

        UserEntity userEntity = new UserEntity();

        try {
            userEntity = userService.getUser(userEmail);
        } catch (Exception e) {
            log.error("Unable to get User: " + userEmail + " - " + e.getMessage());
        }

        if (userEntity != null) {
            log.info(UserManagementSystemConstants.OK);
            return new ResponseEntity(userEntity, new HttpHeaders(), HttpStatus.OK);
        } else {
            log.warn(UserManagementSystemConstants.BAD_REQUEST);
            return new ResponseEntity<>(userEmail, new HttpHeaders(), HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/listUsers")
    public ResponseEntity listUsers() {

        List<UserEntity> userList = null;

        try {
            userList = userService.listUsers();
        } catch (Exception e) {
            log.error("Unable to retrieve User List: " + e.getMessage());
        }

        if (userList != null) {
            log.info(UserManagementSystemConstants.OK);
            return new ResponseEntity<>(userList, new HttpHeaders(), HttpStatus.OK);
        } else {
            log.warn(UserManagementSystemConstants.BAD_REQUEST);
            return new ResponseEntity(new HttpHeaders(), HttpStatus.BAD_REQUEST);
        }

    }

    @PostMapping("/login")
    public ResponseEntity<UserLoginModel> userLogin(@RequestBody UserLoginModel userLoginModel) {

        boolean loginSuccess = false;

        try {
            loginSuccess = userService.userLogin(userLoginModel);
        } catch (Exception e) {
            log.error("Unable to login User with credentials: " + userLoginModel.getEmail() + " - " + e.getMessage());
        }

        if (loginSuccess) {
            log.info(UserManagementSystemConstants.OK);
            return new ResponseEntity<>(userLoginModel, new HttpHeaders(), HttpStatus.OK);
        } else {
            log.warn(UserManagementSystemConstants.BAD_REQUEST);
            return new ResponseEntity<>(userLoginModel, new HttpHeaders(), HttpStatus.BAD_REQUEST);
        }
    }
}
