package com.padraig.usermanagementsystem;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * User Management System Application Runner Class
 *
 * @author Padraig Glynn O'Shaughnessy (pglynnoshaughnessy@gmail.com)
 * @since 01/2021
 */
@SpringBootApplication
public class UserManagementSystemApplication {

    public static void main(String[] args) {
        SpringApplication.run(UserManagementSystemApplication.class, args);
    }

}
