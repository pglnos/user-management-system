package com.padraig.usermanagementsystem.service;

import com.padraig.usermanagementsystem.entity.UserEntity;
import com.padraig.usermanagementsystem.model.UserLoginModel;
import com.padraig.usermanagementsystem.model.UserModel;
import com.padraig.usermanagementsystem.repository.UserRepository;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

/**
 * User Service Test Class
 *
 * @author Padraig Glynn O'Shaughnessy (pglynnoshaughnessy@gmail.com)
 * @since 01/2021
 */
@ExtendWith(MockitoExtension.class)
@DisplayName("User Service Test Class: ")
class UserServiceTest {

    @InjectMocks
    UserService userService;

    @Mock
    UserRepository userRepository;

    private final String userEmail = "pglynnoshaughnessy@gmail.com";
    private final UserModel userModel = new UserModel("pglynnoshaughnessy@gmail.com", "Padraig", "123");
    private final UserLoginModel userLoginModel = new UserLoginModel("pglynnoshaughnessy@gmail.com", "123");
    private final UserEntity userEntity = new UserEntity("pglynnoshaughnessy@gmail.com", "Padraig", "123", null);
    private final List<UserEntity> userList = new ArrayList<>();

    @Test
    void addUser() {

        assertTrue(userService.addUser(userModel));
    }

    @Test
    void editUser() {

        when(userRepository.findByEmail(userModel.getEmail())).thenReturn(userEntity);
        assertTrue(userService.editUser(userModel));
    }

    @Test
    void deleteUser() {

        when(userRepository.findByEmail(userModel.getEmail())).thenReturn(userEntity);
        assertTrue(userService.deleteUser(userEmail));
    }

    @Test
    void getUser() {

        when(userRepository.findByEmail(userModel.getEmail())).thenReturn(userEntity);
        assertEquals(userEntity, userService.getUser(userEmail));
    }

    @Test
    void listUsers() {

        userList.add(userEntity);
        when(userRepository.findAll()).thenReturn(userList);
        assertEquals(userList, userService.listUsers());
    }

    @Test
    void userLogin() {

        when(userRepository.findByEmail(userModel.getEmail())).thenReturn(userEntity);
        assertTrue(userService.userLogin(userLoginModel));
    }
}