package com.padraig.usermanagementsystem;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * User Management System Application Runner Test Class
 *
 * @author Padraig Glynn O'Shaughnessy (pglynnoshaughnessy@gmail.com)
 * @since 01/2021
 */
@DisplayName("User Management System Application Runner Tests: ")
@SpringBootTest
class UserManagementSystemApplicationTests {

    @Test
    @DisplayName("Context Loads")
    void contextLoads() {
    }

}
