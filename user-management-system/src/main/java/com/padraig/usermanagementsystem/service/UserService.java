package com.padraig.usermanagementsystem.service;

import com.padraig.usermanagementsystem.entity.UserEntity;
import com.padraig.usermanagementsystem.model.UserLoginModel;
import com.padraig.usermanagementsystem.model.UserModel;
import com.padraig.usermanagementsystem.repository.UserRepository;
import com.padraig.usermanagementsystem.util.UserUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * User Service Class
 *
 * @author Padraig Glynn O'Shaughnessy (pglynnoshaughnessy@gmail.com)
 * @since 01/2021
 */
@Slf4j
@Service
public class UserService {

    private final UserRepository userRepository;

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public boolean addUser(UserModel userModel) {

        boolean requestComplete = false;

        try {

            UserEntity userEntity = UserUtil.modelToEntity(userModel);

            if (userRepository.findByEmail(userEntity.getEmail()) == null) {

                userRepository.save(userEntity);
                requestComplete = true;
                log.info("User: " + userEntity.getEmail() + " saved to USER table");

            } else {
                log.error("USER email: " + userEntity.getEmail() + " is already present in USER table");
            }

        } catch (Exception e) {

            log.error("User: " + userModel.getEmail() + " not saved to USER table: " + e.getMessage());
            requestComplete = false;
        }

        return requestComplete;
    }

    public boolean editUser(UserModel userModel) {

        boolean requestComplete = false;

        try {

            UserEntity userEntity = userRepository.findByEmail(userModel.getEmail());

            if (userEntity != null) {

                String lastLoginDate = userEntity.getLastLoginDate();
                userEntity = UserUtil.modelToEntity(userModel);
                userEntity.setLastLoginDate(lastLoginDate);
                userRepository.save(userEntity);
                requestComplete = true;
                log.info("User: " + userEntity.getEmail() + " edited in USER table");

            } else {
                log.error("User email: " + userModel.getEmail() + " not found in USER table");
            }

        } catch (Exception e) {

            log.error("User: " + userModel.getEmail() + " not edited in USER table: " + e.getMessage());
        }

        return requestComplete;
    }

    public boolean deleteUser(String userEmail) {

        boolean requestComplete = false;

        try {

            if (userRepository.findByEmail(userEmail) != null) {

                userRepository.deleteByEmail(userEmail);
                requestComplete = true;
                log.info("User: " + userEmail + " deleted from USER table");

            } else {

                log.error("User: " + userEmail + " could not be deleted from USER table");
            }

        } catch (Exception e) {

            log.error("User: " + userEmail + " could not be deleted from USER table: " + e.getMessage());
        }

        return requestComplete;
    }

    public UserEntity getUser(String userEmail) {

        UserEntity userEntity = new UserEntity();

        try {

            userEntity = userRepository.findByEmail(userEmail);

            if (userEntity != null) {

                log.info("User: " + userEmail + " retrieved from USER table");
                return userEntity;

            } else {

                log.error("User: " + userEmail + " is not present in the USER table");
            }
        } catch (Exception e) {
            log.error("Unable to retrieve User: " + userEmail + " from the USER table");
        }

        return userEntity;
    }

    public List<UserEntity> listUsers() {

        List<UserEntity> userList = null;

        try {
            userList = userRepository.findAll();
            log.info("User List received from USER table");
        } catch (Exception e) {
            log.error("Unable to retrieve User List from USER table: " + e.getMessage());
        }

        return userList;
    }

    public boolean userLogin(UserLoginModel userLoginModel) {

        boolean loginSuccess = false;

        try {

            UserEntity userEntity = getUser(userLoginModel.getEmail());

            if (userEntity.getPassword().equals(userLoginModel.getPassword())) {

                userEntity.setLastLoginDate(UserUtil.getTimeAndDate());
                log.info("User: " + userEntity.getEmail() + " last login date updated: " + userEntity.getLastLoginDate());

                userRepository.save(userEntity);
                loginSuccess = true;
                log.info("Login successful for User: " + userLoginModel.getEmail());

            } else {

                log.error("Login failed for User: " + userLoginModel.getEmail());
            }

        } catch (Exception e) {
            log.error("Login failed for user: " + userLoginModel.getEmail() + " - " + e.getMessage());
        }

        return loginSuccess;
    }
}
