package com.padraig.usermanagementsystem.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * User Model Class
 *
 * @author Padraig Glynn O'Shaughnessy (pglynnoshaughnessy@gmail.com)
 * @since 01/2021
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UserModel {

    private String email;
    private String name;
    private String password;
}
