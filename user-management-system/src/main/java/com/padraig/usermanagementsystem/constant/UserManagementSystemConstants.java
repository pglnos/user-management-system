package com.padraig.usermanagementsystem.constant;

/**
 * User Management System Constants Class
 *
 * @author Padraig Glynn O'Shaughnessy (pglynnoshaughnessy@gmail.com)
 * @since 01/2021
 */
public class UserManagementSystemConstants {

    private UserManagementSystemConstants() {
    }

    public static final String CREATED = "Http Status 201 CREATED returned";
    public static final String OK = "Http Status 200 OK returned";
    public static final String BAD_REQUEST = "Http Status 400 BAD_REQUEST returned";
}
